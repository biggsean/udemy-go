package main

import "fmt"

func main() {
        colors := map[string]string{
                "red": "#ff0000",
                "green": "#4bf745",
                "white": "#ffffff",
        }

        fmt.Println(colors)
        printMap(colors)
}

func printMap( m map[string]string) {
        for k, v := range m {
                fmt.Println(k+":\t"+v)
        }
}
