package main

import "fmt"


type square struct{
        sideLength float64
}

type triangle struct{
        height float64
        base float64
}

type shape interface {
        getArea() float64
}

func main() {
        tri := triangle{
                height: 5.0,
                base: 3.0,
        }
        sq := square{
                sideLength: 16.0,
        }

        printArea(tri)
        printArea(sq)
}

func (s square) getArea() float64 {
        return s.sideLength * s. sideLength
}

func (t triangle) getArea() float64 {
        return t.height * t.base * 0.5
}

func printArea(s shape) {
        fmt.Printf("The area is %v\n", s.getArea())
}
