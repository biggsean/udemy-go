package main

import "fmt"

type contactInfo struct {
	email   string
	zipcode int
}
type person struct {
	firstName string
	lastName  string
}

func main() {
	alex := person{"Alex", "Anderson"}
	bilbo := person{firstName: "Bilbo", lastName: "Baggins"}
	var tim person
	fmt.Println(alex)
	fmt.Println(bilbo)
	fmt.Println(tim)
	alex.print()
	bilbo.print()
	tim.print()
}

func (p person) print() {
	fmt.Printf("%+v\n", p)
}
